# homie-ws2812b-mqtt
Control a WS2812B Led Strip with MQTT messages, using Homie library. Locally compiled on a NodeMCU

Supports 4 differents sub topics :
- brightness (0 to 255)
- speed (0-100%)
- mode ("next", "prev", or direct mode number). For list of supported modes, see doc of the WS2812FX library (which I am using to control everything on my leds) here : https://github.com/kitesurfer1404/WS2812FX
- color (RGB)

## Lib dependencies
I'm using PlatformIO for development, and the following dependencies are needed (you may use to install more libs if using Arduino IDE) :

    lib_deps = Homie, WS2812FX

I make an extensive use of the excellent Homie framework (https://github.com/marvinroger/homie-esp8266/)
