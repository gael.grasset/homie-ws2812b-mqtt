#include <Homie.h>
#include <WS2812FX.h>


// Homie Firmware
#define CUSTOM_FIRMWARE_NAME "ws2812b-ledstrips"
#define CUSTOM_FIRMWARE_VERSION "1.0.0"

#define PERCENT_SPEED_MIN 0
#define PERCENT_SPEED_MAX 100

#define DEFAULT_LED_COUNT 15
#define LED_PIN D5

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
WS2812FX ws2812fx = WS2812FX(DEFAULT_LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// Var needed to signal a change of value in the different channels. WS2812FX can't get updated with the new value in the
// Handler since it runs in a separate Thread with Homie... We need buffer variables to push the change in the main loop.
uint8_t runtimeBrightness;
int8_t runtimeMode; // need to be signed to allow mode cycling
uint16_t runtimeSpeed;
uint8_t runtimeColorR;
uint8_t runtimeColorG;
uint8_t runtimeColorB;

bool brightnessHasChanged, modeHasChanged, speedHasChanged, colorHasChanged;

// Homie nodes 
HomieNode led("led", "WS2812B-LED-STRIP"); 

// Homie settings. All optional (see setDefaultValue in setup function)
HomieSetting<long> ledNumber("ledNumber", "Number of LEDS on the strip");
HomieSetting<long> defaultMode("defaultMode", "Default Mode (start at 0, should be in the range of WS2812FX supported modes");
HomieSetting<long> defaultBrightness("defaultBrightness", "Default brightness (0-100)");
HomieSetting<const char*> defaultColor("defaultColor", "Default color, format as 'R, G, B' (ex 0,0,255)");



uint16_t percentSpeed_To_WS2812FXSpeed(uint8_t percentSpeed) {
    uint16_t result = uint16_t(((SPEED_MAX-SPEED_MIN) / 100) * percentSpeed + SPEED_MIN);
    Homie.getLogger() << "percentSpeed_To_WS2812FXSpeed - " << percentSpeed << "% --> " << result << endl;
    return result;
}

uint8_t WS2812FXSpeed_To_percentSpeed(uint8_t WS2812FXSpeed) {
    uint8_t result = uint8_t((WS2812FXSpeed-SPEED_MIN)/(SPEED_MAX-SPEED_MIN)*100);
    Homie.getLogger() << "WS2812FXSpeed_To_percentSpeed - " << WS2812FXSpeed << " --> " << result << "%" << endl;
    return result;
}

// Splits a String based on a separator character
String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

uint8_t getRedValueFromColor(uint32_t c) {
    return c >> 16;
}

uint8_t getGreenValueFromColor(uint32_t c) {
    return c >> 8;
}

uint8_t getBlueValueFromColor(uint32_t c) {
    return c;
}

bool brightnessHandler (const HomieRange& range, const String& value) {
    brightnessHasChanged = true;
    runtimeBrightness = value.toInt();
    runtimeBrightness = max(BRIGHTNESS_MIN, runtimeBrightness);
    runtimeBrightness = min(BRIGHTNESS_MAX, runtimeBrightness);
    Homie.getLogger() << "Brightness changed to " << runtimeBrightness << endl;
    return true;
}

bool speedHandler (const HomieRange& range, const String& value) {
    speedHasChanged = true;
    runtimeSpeed = value.toInt();
    runtimeSpeed = percentSpeed_To_WS2812FXSpeed(runtimeSpeed);
    runtimeSpeed = max(SPEED_MIN, runtimeSpeed);
    runtimeSpeed = min(SPEED_MAX, runtimeSpeed);
    Homie.getLogger() << "Speed changed to " << runtimeSpeed << endl;
    return true;
}

bool modeHandler (const HomieRange& range, const String& value) {
    modeHasChanged = true;
    runtimeMode = 0;	
    if (value.equals("next")) {
		runtimeMode = ws2812fx.getMode() + 1;
	} else if (value.equals("prev")) {
		runtimeMode = ws2812fx.getMode() - 1;;
	} else {
	    runtimeMode = value.toInt();
    }
    if (runtimeMode > MODE_COUNT-1) runtimeMode = 0;
    if (runtimeMode < 0) runtimeMode = MODE_COUNT-1;		
    

    Homie.getLogger() << "Mode changed to " << runtimeMode << " - " << ws2812fx.getModeName(runtimeMode) << endl;
    return true;
}

// TODO put format validations
void updateRuntimeColorsBasedOnStringRGBValues(const String& value) {
    runtimeColorR = getValue(value, ',', 0).toInt();
    runtimeColorG = getValue(value, ',', 1).toInt();
    runtimeColorB = getValue(value, ',', 2).toInt();
}

bool colorRGBHandler (const HomieRange& range, const String& value) {    
    colorHasChanged = true;
    Homie.getLogger() << "New RGB Color : " << value << endl;
    updateRuntimeColorsBasedOnStringRGBValues(value);
    Homie.getLogger() << "Color changed to RGB : " << runtimeColorR << "," << runtimeColorG << "," << runtimeColorB << "," << endl;
    return true;
}

void loopHandler() {    
    if (brightnessHasChanged) {
		ws2812fx.setBrightness(runtimeBrightness);
		brightnessHasChanged = false;
		led.setProperty("brightness").send(String(ws2812fx.getBrightness()));
	}
	if (modeHasChanged) {
		ws2812fx.setMode(runtimeMode);
		modeHasChanged = false;
		led.setProperty("mode").send(ws2812fx.getModeName(ws2812fx.getMode()));
	}
	if (speedHasChanged) {
		ws2812fx.setSpeed(runtimeSpeed);
		modeHasChanged = false;
		led.setProperty("speed").send(String(WS2812FXSpeed_To_percentSpeed(ws2812fx.getSpeed())));
	}
    if (colorHasChanged) {
		ws2812fx.setColor(runtimeColorR, runtimeColorG, runtimeColorB);
		colorHasChanged = false;
		led.setProperty("color").send(
            String(getRedValueFromColor(ws2812fx.getColor())) + "," + 
            String(getGreenValueFromColor(ws2812fx.getColor())) + "," + 
            String(getBlueValueFromColor(ws2812fx.getColor())));
	}
	ws2812fx.service();
}

void ledSetupHandler(){
    ws2812fx.init();
    // Default values are defined in custom Settings
    ws2812fx.setBrightness(defaultBrightness.get());
    ws2812fx.setSpeed(DEFAULT_SPEED);
    ws2812fx.setMode(defaultMode.get());
    updateRuntimeColorsBasedOnStringRGBValues(defaultColor.get());
    ws2812fx.setColor(runtimeColorR, runtimeColorG, runtimeColorB); 
    ws2812fx.setLength(ledNumber.get());
    ws2812fx.start();
    ws2812fx.service();

    // Broadcast setup properties
    led.setProperty("brightness").send(String(ws2812fx.getBrightness()));
    led.setProperty("mode").send(ws2812fx.getModeName(ws2812fx.getMode()));
    led.setProperty("speed").send(String(WS2812FXSpeed_To_percentSpeed(ws2812fx.getSpeed())));
    led.setProperty("color").send(
            String(getRedValueFromColor(ws2812fx.getColor())) + "," + 
            String(getGreenValueFromColor(ws2812fx.getColor())) + "," + 
            String(getBlueValueFromColor(ws2812fx.getColor())));
}

void setup() {
    Serial.begin(115200);
    Homie.getLogger() << endl << endl;
    Homie_setFirmware(CUSTOM_FIRMWARE_NAME, CUSTOM_FIRMWARE_VERSION);

    // Setup topics
    led.advertise("brightness").settable(brightnessHandler);
    led.advertise("speed").settable(speedHandler);
    led.advertise("mode").settable(modeHandler);    
    led.advertise("color").settable(colorRGBHandler);

    // Setup custom settings    
    ledNumber.setDefaultValue(DEFAULT_LED_COUNT).setValidator([] (long candidate) {
        return candidate > 0;
    });
    defaultMode.setDefaultValue(FX_MODE_STATIC).setValidator([] (long candidate) {
        return candidate >= 0 && candidate < MODE_COUNT;
    });
    defaultBrightness.setDefaultValue(DEFAULT_BRIGHTNESS).setValidator([] (long candidate) {
        return candidate >= 0 && candidate <= 100;
    });

    // End of setup
    Homie.setSetupFunction(ledSetupHandler);
    Homie.setLoopFunction(loopHandler);
    Homie.setup();
}

void loop() {
    Homie.loop();
}